/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.subject.facultation.model.service.impl;

import in.ac.gpckasaragod.subject.facultation.model.service.SubjectService;
import in.ac.gpckasaragod.subject.facultation.ui.model.data.Subject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextField;

/**
 *
 * @author student
 */
 public class SubjectsServiceImpl extends ConnectionServiceImpl implements SubjectService {
    @Override
    public String saveSubject(String name, String code) {
       // throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
  
     try {
           Connection connection = getConnection();
           Statement statement = connection.createStatement();
           String query = "INSERT INTO SUBJECT (NAME,CODE) VALUES"
                         + "('" + name + "','" + code + "',)";
           System.err.println("Query:" + query);
           int status =  statement.executeUpdate(query);
           if(status!= 1) {
               return "save failed";
           } else {
               return "save successfully";
               
               
           }
     }  catch (java.sql.SQLException ex) {
            Logger.getLogger(SubjectsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            return "Save failed";
            
        }
    }
    @Override
        public Subject readSubject(Integer id)  {
         Subject subject;
         subject = null;
      try{
         Connection connection = getConnection();
         Statement statement = connection.createStatement();
         String query = "SELECT * FROM SUBJECT WHERE ID=" + id;
         ResultSet resultSet = statement.executeQuery(query);
         while (resultSet.next()) {
             id = resultSet.getInt("ID");
             String name = resultSet.getString("NAME");
             String code = resultSet.getString("CODE");
             subject = new Subject(id,name,code);
             }
         }catch (SQLException ex) {
             Logger.getLogger(SubjectsServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
             
         }
      return subject;
  }

   

    @Override
    public List<Subject> getALLSubjects() {
        List<Subject> subjects = new ArrayList<>();
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM SUBJECT";
            ResultSet resultSet = statement.executeQuery(query);
            
            while (resultSet.next()) {
                Integer id = resultSet.getInt("ID");
                String name = resultSet.getString("NAME");
                String code = resultSet.getString("CODE");
                Subject subject = new Subject(id,name,code);
                subjects.add(subject);
            }
        }catch (SQLException ex) {
            Logger.getLogger(SubjectsServiceImpl .class.getName()).log(Level.SEVERE,null,ex);
        }
        return subjects;
        
        }
                
     
        
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBod

    @Override
    public String updateSubject(Integer id,String name, String code ) {
        try {
             Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "UPDATE SUBJECT SET NAME='"+name +",CODE='"+code+"'WHERE ID=" + id;
            System.out.print(code);
            int update = statement.executeUpdate(query);
            if (update !=1) {
                return "Update failed";
            } else {
                return "Updated successfully";
                
            }

        }catch (SQLException ex){
            Logger.getLogger(SubjectsServiceImpl .class.getName()).log(Level.SEVERE,null,ex);
        }
        return "Updated failed";
        }
    @Override
    public String deleteSubject(Integer id) {
         
        try {
            Connection connection = getConnection();
            String query ="DELETE FROM SUBJECT WHERE ID=?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1,id);
            int delete =statement.executeUpdate();
            if(delete !=1)
                return "Delete failed";
            else 
                return "Delete successfully";
        }catch (SQLException ex) {
            Logger.getLogger(SubjectsServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
        }
            return "Delete failed";
        
      }


    
    }

    