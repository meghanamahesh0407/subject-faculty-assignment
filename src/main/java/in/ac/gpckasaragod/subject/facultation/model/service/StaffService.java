/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.subject.facultation.model.service;

import in.ac.gpckasaragod.subject.facultation.ui.model.data.Staff;
import in.ac.gpckasaragod.subject.facultation.ui.model.data.Subject;
import java.util.List;
import javax.swing.JTextField;

/**
 *
 * @author student
 */
public interface StaffService {
     public String saveStaff(String name,String subject);
    public Staff readStaff(Integer id);
public List <Staff>getALLStaffs();
public String updateStaff(Integer iD,String name,String subject);
public String deleteStaff(Integer id);

    public String saveService(String name, String code);

    public String updateSubject(Integer selectedId, String name, JTextField txtsubject);

    public List<Staff> getALLStaff();

}