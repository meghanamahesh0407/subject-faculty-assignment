/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.subject.facultation.model.service;

import in.ac.gpckasaragod.subject.facultation.ui.model.data.Subject;
import java.util.List;
import javax.swing.JTextField;


/**
 *
 * @author student
 */
public interface SubjectService {
    public List <Subject>getALLSubjects();
    public String updateSubject(Integer iD,String name,String code);
    public String deleteSubject(Integer id);

    public String saveSubject(String name, String code);

   
    public Subject readSubject(Integer id);

    
    
}